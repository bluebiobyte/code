#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Daniel Rode
# Name: Backup Sync
# Tags: backup, file management, data management
# Dependencies:
#    Python3.7+
#    restic
#    getcred
#    ncdu
# Version: 30
# Created: 23 Nov 2022
# Updated: 04 Feb 2025


# Description: Backs up my data online using restic.

# NOTE: This script loads all needed credentials except for the SSH key.
# Accessing repos on rsync.net requires SSH access. Public/private key
# authentication is currently setup for several of my hosts. If I ever
# reinstall these hosts or add a new host, SSH private/public key
# authentication will need to be setup on that host before this script will
# work (otherwise, the user will be prompted to enter a password after restic
# is called).

# NOTE: In order to make this script functional under Termux (on Android), it
# must be run under a functional proot environment. This is probably already
# setup, but in case it isn't, see comments in 'enter-void.sh' under Termux 
# home directory.

# NOTE: This script requires getcred can get the master password from the
# user. Under termux, this is difficult. In order to work around this, start
# getcred before calling this script. Use:
#  dtach -c `mktemp -d`/sock getcred --server --password-prompt-fallback 
# Enter the master KeePass credential password when prompted, then press
# Ctrl+\ to detach, letting getcred continue on in the background.


import os
import sys
import pathlib as pl
import subprocess as sp
import datetime as dt

from sys import exit


# Variables
HOME_PATH = pl.Path.home()
SDCARD_PATH = pl.Path('/sdcard')
HOSTNAME = os.environ['HOSTNAME']

universal_exclude_patterns = [
    # Directories
    str(HOME_PATH / '.AndroidStudio3.0/'),
    str(HOME_PATH / '.cache/'),
    str(HOME_PATH / '.cargo/'),
    str(HOME_PATH / '.claws-mail/imapcache/'),
    str(HOME_PATH / '.claws-mail/tmp/'),
    str(HOME_PATH / '.conda/pkgs/'),
    str(HOME_PATH / '.config/0ad/logs/'),
    str(HOME_PATH / '.config/Slack/'),
    str(HOME_PATH / '.config/chromium/'),
    str(HOME_PATH / '.config/discord/'),
    str(HOME_PATH / '.config/google-chrome/'),
    str(HOME_PATH / '.config/libreoffice/'),
    str(HOME_PATH / '.config/microsoft-edge/'),
    str(HOME_PATH / '.config/pulse/'),
    str(HOME_PATH / '.config/sublime-text-3/Cache/'),
    str(HOME_PATH / '.config/sublime-text-3/Installed Packages/Package Control.sublime-package/'),
    str(HOME_PATH / '.config/sublime-text-3/Packages/User/Package Control.cache/'),
    str(HOME_PATH / '.config/sublime-text/Packages/regex/'),
    str(HOME_PATH / '.config/sublime-text/Packages/requests/'),
    str(HOME_PATH / '.config/syncthing/index-v*.db/'),
    str(HOME_PATH / '.config/zsh/.zprezto/.git/'),
    str(HOME_PATH / '.dropbox/'),
    str(HOME_PATH / '.dropbox-dist/'),
    str(HOME_PATH / '.emacs.d/'),
    str(HOME_PATH / '.flowblade/thumbnails/'),
    str(HOME_PATH / '.googleearth/Cache/'),
    str(HOME_PATH / '.gradle/'),
    str(HOME_PATH / '.julia/'),
    str(HOME_PATH / '.klei/'),
    str(HOME_PATH / '.local/lib/'),
    str(HOME_PATH / '.local/opt/MultiMC/assets/'),
    str(HOME_PATH / '.local/opt/MultiMC/bin/'),
    str(HOME_PATH / '.local/opt/MultiMC/cache/'),
    str(HOME_PATH / '.local/opt/MultiMC/libraries/'),
    str(HOME_PATH / '.local/opt/MultiMC/meta/'),
    str(HOME_PATH / '.local/opt/MultiMC/mods/'),
    str(HOME_PATH / '.local/opt/MultiMC/plugins/'),
    str(HOME_PATH / '.local/opt/MultiMC/translations/'),
    str(HOME_PATH / '.local/opt/MultiMC/update/'),
    str(HOME_PATH / '.local/opt/MultiMC/versions/'),
    str(HOME_PATH / '.local/opt/context/'),
    str(HOME_PATH / '.local/share/Anki/QtWebEngine/'),
    str(HOME_PATH / '.local/share/PrismLauncher/assets/'),
    str(HOME_PATH / '.local/share/PrismLauncher/libraries/'),
    str(HOME_PATH / '.local/share/Steam/'),
    str(HOME_PATH / '.local/share/TelegramDesktop/'),
    str(HOME_PATH / '.local/share/Trash/'),
    str(HOME_PATH / '.local/share/chaiNNer/'),
    str(HOME_PATH / '.local/share/containers/'),
    str(HOME_PATH / '.local/share/fish/generated_completions/'),
    str(HOME_PATH / '.local/share/geeqie/trash/'),
    str(HOME_PATH / '.local/share/gem/'),
    str(HOME_PATH / '.local/share/gvfs-metadata/'),
    str(HOME_PATH / '.local/share/jan/extensions/'),
    str(HOME_PATH / '.local/share/jan/models/'),
    str(HOME_PATH / '.local/share/nvim/lazy/'),
    str(HOME_PATH / '.local/share/nvim/mason/'),
    str(HOME_PATH / '.local/share/QGIS/QGIS3/profiles/default/proj/'),
    str(HOME_PATH / '.local/share/qutebrowser/pdfjs/'),
    str(HOME_PATH / '.local/share/qutebrowser/qtwebengine_dictionaries/'),
    str(HOME_PATH / '.local/share/qutebrowser/webengine/'),
    str(HOME_PATH / '.local/share/themes/'),
    str(HOME_PATH / '.local/share/tldr/'),
    str(HOME_PATH / '.local/share/torbrowser/tbb/'),
    str(HOME_PATH / '.local/share/tracker/'),
    str(HOME_PATH / '.local/share/waydroid/'),
    str(HOME_PATH / '.lyx/cache/'),
    str(HOME_PATH / '.minecraft/assets/'),
    str(HOME_PATH / '.minecraft/libraries/'),
    str(HOME_PATH / '.mozilla/**/saved-telemetry-pings/'),
    str(HOME_PATH / '.mozilla/**/storage/'),
    str(HOME_PATH / '.npm/'),
    str(HOME_PATH / '.ollama/models/blobs/'),
    str(HOME_PATH / '.shheekretts/lost+found/'),
    str(HOME_PATH / '.steam/'),
    str(HOME_PATH / '.texlive20*/'),
    str(HOME_PATH / '.thumbnails/'),
    str(HOME_PATH / '.thunderbird/'),  # TODO I need to implement way to backup my email, like perhaps by syncing thunderbird mbox to a maildir, then backup the maildir
    str(HOME_PATH / '.var/app/com.microsoft.Edge/'),
    str(HOME_PATH / '.var/app/io.mrarm.mcpelauncher/'),
    str(HOME_PATH / '.var/app/org.kde.falkon/cache/'),
    str(HOME_PATH / '.var/app/org.qgis.qgis/cache/'),
    str(HOME_PATH / '.zoom/'),
    str(HOME_PATH / 'Dropbox/'),
    str(HOME_PATH / 'downloads/'),
    str(HOME_PATH / 'job/csu_nrel_jody/private_data/'),
    str(HOME_PATH / 'progeny/android-studio/sdk/'),
    str(HOME_PATH / 'progeny/go/'),
    str(HOME_PATH / 'progeny/gocryptfs/dotcrypt/'),
    str(HOME_PATH / 'progeny/virt-manager/'),
    str(HOME_PATH / 'record/import_temp/'),
    str(HOME_PATH / 'store/disk images/operating systems/'),
    str(HOME_PATH / 'store/luks-home'),
    str(HOME_PATH / 'union/csu-nrel/'),
    str(HOME_PATH / 'union/github/'),
    str(HOME_PATH / 'union/occupation/csu_nrel_jody/private_data/'),
    str(HOME_PATH / 'union/sch_laptop/'),
    str(HOME_PATH / 'warehouse/disk images/operating systems/'),

    # Files
    str(HOME_PATH / '.ICEauthority'),
    str(HOME_PATH / '.config/nnn/.selection'),
    str(HOME_PATH / '.esd_auth'),
    str(HOME_PATH / '.local/share/qutebrowser/history.sqlite*'),
    str(HOME_PATH / '.local/state/nvim/log'),
    str(HOME_PATH / '.local/var/log/syncthing-audit.log'),
    str(HOME_PATH / '.local/var/log/syncthing-main.log'),
    str(HOME_PATH / '.sway-session-stderr.log'),
    str(HOME_PATH / '.sway-session-stderr.log.old'),
    str(HOME_PATH / '.sway-session-stdout.log'),
    str(HOME_PATH / '.sway-session-stdout.log.old'),
]

# Host specific values for restic
host_gramvoidd = {
    'cred path': 'programs/restic/gramvoidd',
    'repo address': lambda: (f'sftp:{RESTIC_USERNAME}@{RESTIC_USERNAME}'
        '.rsync.net:restic-gramvoidd'),
    'backup paths': [HOME_PATH],
    'exclude patterns': [
        # Directories
        '/home/daniel/working/nerd-dictation/model',
    ],
}
host_andtermuxdvoid = {
    'cred path': 'programs/restic/oneplus',
    'repo address': lambda: (f'sftp:{RESTIC_USERNAME}@{RESTIC_USERNAME}'
        '.rsync.net:restic-oneplus'),
    'backup paths': [
        '/etc',
        HOME_PATH,
        SDCARD_PATH / 'abode',
        SDCARD_PATH / 'ArtFlow',
        SDCARD_PATH / 'Audio Recordings',
        SDCARD_PATH / 'Bluecoins',
        SDCARD_PATH / 'Download/library',
        SDCARD_PATH / 'Download/stagnate',
        SDCARD_PATH / 'MyAppList',
        SDCARD_PATH / 'Notebloc',
        SDCARD_PATH / 'Pictures',
        SDCARD_PATH / 'Snapseed',
    ],
    'exclude patterns': [
        # Directories
        '/data/data/com.termux/files/home/.cache',
        '/data/data/com.termux/files/home/.cargo',
        '/data/data/com.termux/files/home/.emacs.d',
        '/data/data/com.termux/files/home/.local/lib',
        '/data/data/com.termux/files/home/.local/opt',
        '/data/data/com.termux/files/home/.local/share/gvfs-metadata',
        '/data/data/com.termux/files/home/.local/share/tldr',
        '/data/data/com.termux/files/home/.local/share/tracker',
        '/data/data/com.termux/files/home/.local/share/Trash',
        '/data/data/com.termux/files/home/.lyx/cache',
        '/data/data/com.termux/files/home/.mozilla',
        '/data/data/com.termux/files/home/.texlive20*',
        '/data/data/com.termux/files/home/.thumbnails',
        '/data/data/com.termux/files/home/downloads',
        '/data/data/com.termux/files/home/progeny/go',
        '/data/data/com.termux/files/home/progeny/virt-manager',
        '/data/data/com.termux/files/home/record/import_temp',
        '/sdcard/abode/dropbox',

        # Files
        '/data/data/com.termux/files/home/.config/nnn/.selection',
        '/data/data/com.termux/files/home/.esd_auth',
        '/data/data/com.termux/files/home/.ICEauthority',
    ],
}


# Function
def ncduX(exclude_patterns):
    # Run this function to get an idea of what large directories/files to
    # exclude. The command runs ncdu on your home directory and tells it to
    # ignore paths that match patterns in the given exclusion list.

    exclude_pattern_args = list()
    for pattern in exclude_patterns:
        exclude_pattern_args.append('--exclude')
        exclude_pattern_args.append(pattern)

    cmd = ['ncdu', *exclude_pattern_args, HOME_PATH]
    sp.run(cmd, check=True)

def log_system_state(host):
    pass
    #TODO


# Determine host
if HOSTNAME == 'gramvoidd':
    print("Host 'gramvoidd' detected")
    host = host_gramvoidd
elif HOSTNAME == 'andtermuxd-void':
    print("Host 'andtermuxd-void' detected")
    host = host_andtermuxdvoid
else:
    print("error: Unsupported host")
    exit(1)


# Parse command line input
DRY_RUN_MODE = False
args = iter(sys.argv[1:])
for i in args:
    if i in ['-X', '--ncdu']:
        ncduX(host['exclude patterns'] + universal_exclude_patterns)
        exit()
    elif i in ['-d', '--dry-run']:
        DRY_RUN_MODE = True
        print("Running in dry run mode")
    else:
        print("error: Unsupported argument")
        exit(1)


# Collect credentials for restic
print("Loading credentials...")

credentials = dict()
credentials['RESTIC_PASSWORD_COMMAND'] = \
    f'getcred {host["cred path"]} password'
p = sp.run(
    ['getcred', host['cred path'], 'username'],
    check=True, text=True, capture_output=True
)
RESTIC_USERNAME = p.stdout

# From `backup-dtideapad.py` (useful reference for how to setup connection to
# B2 repo, if I ever decide to back up to that service again):
# cred_path = "programs/restic/dtideapad/B2"
# cred_secrets = dict()
# cred_secrets['RESTIC_PASSWORD_COMMAND'] = f'getcred {cred_path} password'
# cred_secrets['B2_ACCOUNT_KEY'] = str(sp.Popen(
#     ['getcred', cred_path, 'Application Key'],
#     stdout=sp.PIPE).communicate()[0], 'utf8')
# cred_secrets['B2_ACCOUNT_ID'] = str(sp.Popen(
#     ['getcred', cred_path, 'Key ID'],
#     stdout=sp.PIPE).communicate()[0], 'utf8')
# cred_secrets['RESTIC_REPOSITORY'] = str(sp.Popen(
#     ['getcred', cred_path, 'username'],
#     stdout=sp.PIPE).communicate()[0], 'utf8')


# Compile exclude pattern arguments for restic
exclude_pattern_args = list()
for pattern in (host['exclude patterns'] + universal_exclude_patterns):
    exclude_pattern_args.append('-e')  # restic flag: -e = --exclude
    exclude_pattern_args.append(pattern)


# Finish structuring restic command array
repo_addr = host['repo address']
if callable(repo_addr):
    # 'lambda' (which defines anonymous--nameless--functions) is used under
    #  the 'host' dictionary for items that depend on the contents of other
    #  variables that are not yet defined. 'lambda' allows these other
    #  variables to be mentioned in the 'host' dictionary before they are
    #  defined (because functions are do not call the variables they
    #  reference until the function is run). For the 'repo address' key, where
    #  the value does not depend on other variables, the value will just be a
    #  plain string (and thus does not need to be called as a function).
    repo_addr = repo_addr()

cmd = [
    'restic', 'backup',
    '--repo', repo_addr,
    *exclude_pattern_args,
    *host['backup paths'],
]

if DRY_RUN_MODE:
    cmd.insert(2, '--verbose=2')
    cmd.insert(2, '--dry-run')


# Backup
print("Current time:", dt.datetime.now())
#TODO
#print("Saving system state to log files for backup...")
#log_system_state(host)
print("Performing backup...")
p = sp.run(cmd, check=True, env={**os.environ,**credentials})



"""
TODO
- add --help text
- replace ncdu with utility that can take multiple input paths
    - must support interactively browsing tree, and exclusion patterns
- save some system state information:
    - recursive ls of downloads directory (but not the contents themselves)
    - root (non-recursive) ls of /opt
    - ls of /usr/local/bin
    - ls of ~/.local/bin and ~/.local/opt/bin
    - user id
    - hostname
    - services: `sudo vsv`
"""
