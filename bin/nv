#!/usr/bin/env dash
# Author: Daniel Rode
# Tags: macro, wrapper, launcher, files, documents
# Dependencies:
#   nvim (neovim)
#   goneovim?
#   alacritty?
#   neovim-qt?
#   wezterm?
#   wezterm-terminfo?
#   neovide?
#   socat
# Version: 19
# Init: 29 Nov 2022
# Updated: 26 Sep 2023

# Description: Open a given set of files in current instance of
# Neovide/Neovim. If one does not exist, create one.

# Variables
help_text="Usage: ${0##*/} PATH..."
program_name="nv"

[ -z "$XDG_CACHE_HOME" ] && XDG_CACHE_HOME="$HOME/.cache"
CACHE_HOME="$XDG_CACHE_HOME/daniel_rode_code/$program_name"

# Ready environment
SESSION_DIR="$CACHE_HOME/sessions.link"
if [ ! -e "$SESSION_DIR" ]; then
	mkdir -p "$CACHE_HOME"
	ln -sf $(mktemp -d) "$SESSION_DIR"
fi

# Get session ID, create if undefined
if [ -z "$NV_SESSION_ID" ]; then
	export NV_SESSION_ID="$(mktemp -d --tmpdir="$SESSION_DIR")"
	mkdir -p "$NV_SESSION_ID"
fi

export NVIM_LISTEN_ADDRESS="$NV_SESSION_ID/nvim.socket"

# Main

# Start nvim session if it is not running
# https://unix.stackexchange.com/a/556790/33109
# Socat is used to test if socket is currently open or dead
if ! socat -u OPEN:/dev/null UNIX-CONNECT:"$NVIM_LISTEN_ADDRESS" 2>/dev/null; then
	if [ "$NV_NEW_WINDOW" = true ]; then
		# NOTE: whenever I update something here, I need to update
		# `arrange-wins-swaywm.py`
		#
		# Cons: ctrl+enter shortcut does not work
		# alacritty --command nvim -p "$@" &
		# OR
		# Cons: seems to cause issue with spellchecking, scrolls too quick via
		# mouse wheel, crashes when cutting large visual selection of text
		goneovim -p "$@" &
		# OR
		# neovide -- -c 'set guifont=FiraCode\ Nerd\ Font:h11' -p "$@" &
		# OR
		# Pros: ligatures, more keymappings (not bound by terminal key
		# interpretation limitations)
		# Cons: clips large italic letters at ends of line
		# nvim-qt --no-ext-tabline --no-ext-popupmenu -- -p "$@" &
		# /usr/local/bin/nvim-qt -- -p "$@" &  # compiled from source
		# OR
		# Dependency: term = "wezterm" must be set in wezterm.lua config file
		# or else squiggly red underline in neovim will not work
		# Cons: red squigggly underline looks ugly and itallic font is blurry
		# wezterm start nvim -p "$@" &

	else
		[ -t 1 ] || exit 1 # If not interactive terminal, abort
		nvim -p "$@"
	fi

# Open files in existing session
else
	# path="$(realpath "$arg")"
	nvim \
		--server "$NVIM_LISTEN_ADDRESS" \
		--remote "$@"
fi

# TODO
# - ...
