foot-helix --new-window  # Text Editor (New Window)
foot-helix  # Text Editor
firefox  # Web Browser
ms-edge  # Microsoft Edge Web Browser (for Work)
nn  # New Note
thunderbird  # Email
chromium  # Web Browser
xournalpp  # PDF Editor
gimp  # Image Editor
inkscape  # Vector Graphic Editor
meld  # Text Diff
audacity  # Audio Editor
mtpaint  # Lightweight Image Editor
pavucontrol-qt  # Audio Mixer, Sound, Volume
wezterm  # Terminal
foot  # Terminal
latest-screenshot  # View Latest Screenshots
libreoffice --nologo --calc  # Spreadsheet
virt-manager  # Virtual Machine Manager
remmina  # Remote Desktop Viewer (VNC)
simple-scan  # Document Scanner
grimshot --cursor save area - | wl-copy -t image/png  # Screenshot region to clipboard
grimshot --cursor save active - | wl-copy -t image/png  # Screenshot current window to clipboard
grimshot --cursor save screen - | wl-copy -t image/png  # Screenshot full screen to clipboard
grimshot --notify --cursor save area  # Screenshot region to disk
grimshot --notify --cursor save active  # Screenshot current window to disk
grimshot --notify --cursor save screen  # Screenshot full screen to disk
qrscan-screen  # Copy contents of on-screen bar codes to clipboard
c="$(hyprpicker)" && notify-send "$c" && wl-copy "$c"  # Color Picker
wl-paste | strip-newline | wl-copy  # Clipboard Strip Newline
bluetoothctl power off  # Turn off Bluetooth
im passive  # Session Idle Management: Passive
im open  # Session Idle Management: Dim Unlocked
im secure  # Session Idle Management: Dim and Lock
pactl set-sink-mute '@DEFAULT_SINK@' toggle  # Toggle mute/unmute
wconnect b 'F8:B2:17:AE:3B:2A'  # Bluetooth: Connect to Logitech Pebble (mouse)
wconnect b '88:D0:39:AC:92:80'  # Bluetooth: Connect to Soundcore Life Q20 (headphones)
wconnect b '5F:01:EA:99:12:3E'  # Bluetooth: Connect to MusiBaby (speaker)
wconnect w 'Ultra Generic'  # WiFi: Connect to "Ultra Generic" (hotspot)
wconnect w 'csu-eid'  # WiFi: Connect to "csu-eid"
qrgen "$(wl-paste | strip-newline)"  # Display clipboard contents as qrcode
notify-send 'Words: '`wl-paste | wc -w`  # Word count clipboard notification
kmonad ~/code/cfg/kmonad/colemak2qwerty.kbd  # Keyboard Layout: Qwerty (qwfpgj)
footclient sway-rename-workspace  # Sway: Rename Workspace
launch-obs  # Screen Recorder/Streamer
font-manager
steam  # Games
kdenlive  # Video Editor
keepassxc  # Passwords/Credentials
wl-paste | mutate unwrap | wl-copy --trim-newline  # Flatten Clibpoard Text
wl-paste | mutate title | wl-copy --trim-newline  # Title-Case Clipboard Text
conda run --name base foot  # Anaconda Shell Environment
conda run --name base rstudio  # R IDE
zim  # Wiki
nerd-up  # Dictation
flatpak run net.ankiweb.Anki  # Anki (Flashcards)
alacritty  # Terminal
geogebra  # Calculator
flatpak run us.zoom.Zoom  # Zoom (Video Chat)
googleearth  # Maps, GIS
audacious  # Music Player
pomodoro  # Eye Timer
qgis  # QGIS
system-config-printer  # Manage Printers
"$HOME/job/csu_nrel_jody/bin/init-workspace.sh"  # NREL (CSU) job workspace
prismlauncher  # Minecraft (Game)
redx  # Overlay Red Crosshair
wl-copy 'https://www.amazon.com/dp/'`wl-paste | sed 's,.*/dp/,,' | sed 's,[/\?].*,,'`  # Shorten Amazon link in clipboard
nm-connection-editor  # Network Manager (wifi, eth)
nvn  # Neovim via Neovide (Text Editor)
nwg-look  # Desktop Environment GTK Theme Settings
wlsunset -s 00:00 -t 5000 -g 1.5  # Nice screen (gentle contrast and temp)
dim  # Dim screen (reduce brightness)
keymapp  # Moonlander Keyboard
sudo vsv up waydroid-container && waydroid app launch eu.faircode.email  # FairEmail (Android app)
foot bluetuith  # Bluetooth Manager
jan  # Local AI Chat
wl-paste | tr '\\' '/' | wl-copy  # Fix path slashes
remote-host mesa-qgis  # Remote Desktop CSU NREL Mesa Workstation App
remote-host mesa-term  # Remote Desktop CSU NREL Mesa Workstation Foot Term
remote-host mesa  # Remote Desktop (VNC) CSU NREL Mesa Workstation
remote-host rocky  # Remote Desktop (RDP) CSU NREL Workstation
remote-host denali  # Remote Desktop (RDP) CSU NREL Workstation
openhv # flatpak run io.github.openhv.OpenHV  # OpenHV (game)
openra-ra  # OpenRA (game)
flatpak run net.minetest.Minetest  # Luanti
QOwnNotes  # Work (Jody) Notes
hedgewars  # Game
fix-sway-displays  # Fix misconfigured displays on Sway
calibre  # E-Books
