#!/usr/bin/env bash
# Updated: 12 Nov 2024


XDG_CONFIG_HOME="$HOME/.config"
DCV_CODE_PATH="$HOME/code"


cd "$(dirname "$0")"  # Set directory containing this script as CWD


function ln_ {
  ln --backup --symbolic --force --verbose "$(realpath "$1")" "$2"
}
function mkdir_ {
  mkdir --parents "$1"
}


# Make directories
mkdir_ "$HOME/.ipython/profile_default/startup/"
mkdir_ "$XDG_CONFIG_HOME/fish/"
mkdir_ "$XDG_CONFIG_HOME/foot/"
mkdir_ "$XDG_CONFIG_HOME/goneovim/"
mkdir_ "$XDG_CONFIG_HOME/imv/"
mkdir_ "$XDG_CONFIG_HOME/lf/"
mkdir_ "$XDG_CONFIG_HOME/mpv/"
mkdir_ "$XDG_CONFIG_HOME/nushell/"
mkdir_ "$XDG_CONFIG_HOME/tiny/"
mkdir_ "$XDG_CONFIG_HOME/wezterm/"
mkdir_ "$XDG_CONFIG_HOME/zathura/"
mkdir_ "$XDG_CONFIG_HOME/zsh/"


# LINKS
# setup-links.sh         none
# git-global             none: Set path in ~/.gitconfig (see below)
# i3status-rs            none: Path passed to program
# launch                 none: Passed to program
# sway                   none: Paths passed to program
# kmonad                 none: Paths passed to program
# ln_ git-repo-blob_text_journal "$HOME/record/blob/t/.git/config"
# ln_ byobu                ???
# ln_ config.fish          "$XDG_CONFIG_HOME/fish/config.fish"
# ln_ emacs-init.el        ???
# ln_ goneovim.toml        "$XDG_CONFIG_HOME/goneovim/settings.toml"
# ln_ qutebrowser          ???
ln_ alacritty            "$XDG_CONFIG_HOME/"
ln_ empty                "$XDG_CONFIG_HOME/nushell/config.nu"
ln_ foot.ini             "$XDG_CONFIG_HOME/foot/foot.ini"
ln_ git-global/"$(hostname)" \
  "$DCV_CODE_PATH/cfg/git-global/host-specific"
ln_ git-repo-code/base   "$DCV_CODE_PATH/.git/config"
ln_ git-repo-code/"$(hostname)" \
  "$DCV_CODE_PATH/cfg/git-repo-code/host-specific"
ln_ gpg-agent.conf       "$HOME/.gnupg/gpg-agent.conf"
ln_ helix                "$XDG_CONFIG_HOME/"
ln_ imv.conf             "$XDG_CONFIG_HOME/imv/config"
ln_ ipython-init.py \
  "$HOME/.ipython/profile_default/startup/10-user-init.py"
ln_ kanshi               "$XDG_CONFIG_HOME/kanshi/config"
ln_ lfrc                 "$XDG_CONFIG_HOME/lf/lfrc"
ln_ lintr                "$HOME/.lintr"
ln_ mpv-input.conf       "$XDG_CONFIG_HOME/mpv/input.conf"
ln_ nushell.nu           "$XDG_CONFIG_HOME/nushell/env.nu"
ln_ nvim                 "$XDG_CONFIG_HOME/"
ln_ profile              "$HOME/.profile"
ln_ rc.elv               "$XDG_CONFIG_HOME/elvish/rc.elv"
ln_ starship.toml        "$XDG_CONFIG_HOME/starship.toml"
ln_ sway                 "$XDG_CONFIG_HOME/"
ln_ tiny.yml             "$XDG_CONFIG_HOME/tiny/config.yml"
ln_ waybar               "$XDG_CONFIG_HOME/"
ln_ wezterm.lua          "$XDG_CONFIG_HOME/wezterm/wezterm.lua"
ln_ zathura.conf         "$XDG_CONFIG_HOME/zathura/zathurarc"
ln_ zellij.kdl           "$XDG_CONFIG_HOME/zellij/config.kdl"
ln_ zshrc                "$XDG_CONFIG_HOME/zsh/.zshrc"
ln_ sway/gramvoidd-xdg-desktop-portal-wlr "$XDG_CONFIG_HOME/"


# Notify user of any manual config paths/options they should set
echo "Manual actions:"
echo "|----"
echo "Add to your ~/.gitconfig:"
echo '[include]'
echo '	path = ~/code/cfg/git-global/base'
echo


# Check if any of my config files are not handled above and notify
for p in *
do
  if ! grep --silent "$p" "$(basename "$0")"
  then
    echo "warning: Unhandled config file: $(realpath "$p")"
  fi
done
