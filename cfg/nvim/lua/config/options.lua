-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

local opt = vim.opt

-- Settings

-- Set soft line wrap to indent like Sublime Text
opt.breakindent = true

-- Enable line numbering
opt.number = true

-- Ignore case for searches and search/replace
opt.ignorecase = true

-- Set default text width (ruler)
opt.textwidth = 78

-- Use system clipboard by default (rather than internal clipboard)
opt.clipboard = "unnamedplus"

-- Set window title
opt.title = true

-- Soft-wrap text at word boundaries
opt.wrap = true
opt.linebreak = true

-- Set indentation to be 4 spaces
-- opt.shiftwidth = 4
-- opt.tabstop = 4
-- opt.softtabstop = 0
-- opt.expandtab = true

-- Do not echo filepath upon opening file
opt.shortmess:append({ F = true })

-- Move left/right at line edge advances to next/previous line
opt.whichwrap:append("<")
opt.whichwrap:append(">")
opt.whichwrap:append("[")
opt.whichwrap:append("]")

-- Keep 3 lines below/above the cursor and top/bottom of screen
opt.scrolloff = 3

-- Spelling
opt.spellfile = os.getenv("HOME") .. "/.local/share/nvim/personal.utf-8.add" -- Custom dictionary
opt.spellsuggest = "best,9"
