-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here

local au = vim.api.nvim_create_autocmd

-- Don't ruler certain file types
au("BufReadPre", {
  pattern = { "*.md", "*.txt", "*.todo" },
  command = "setlocal textwidth=0",
})
-- TODO change this to work based on filetype rather than file extension

-- Set Dash files to use sh syntax
au("BufRead", {
  pattern = "*",
  command = "if getline(1) == '#!/bin/dash' | set filetype=sh | endif",
})
au("BufRead", {
  pattern = "*",
  command = "if getline(1) == '#!/usr/bin/env dash' | set filetype=sh | endif",
})

-- Set AsciiDoc to use '//' as comment string
au("FileType", {
  pattern = { "asciidoc" },
  command = "setlocal commentstring=//\\ %s",
})

-- Use todo.txt syntax for files
-- requires https://github.com/danielrode/todo.txt-vim plugin
au("BufReadPre", {
  pattern = { "*.todo" },
  command = "set syntax=todo",
})

-- Unset syntax highlighting for Typst (TODO until correct definition file is
-- found and installed)
au("BufRead", {
  pattern = { "*.typ" },
  command = "set filetype=typst",
})
