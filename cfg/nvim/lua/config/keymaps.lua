-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- vim.keymap.set({'n', 't'}, '<A-b>', function() fterm_nnn:toggle() end)

local bind = vim.keymap.set

-- Quit neovim
bind({ "n", "i", "v" }, "<C-q>", "<ESC>:qa<CR>")

-- Set capital letter of undo key as redo (in normal mode)
bind("n", "U", "<C-r>")

-- Alt+Up/Down to move current line up/down
bind("n", "<A-Up>", ":m -2<CR>")
bind("n", "<A-Down>", ":m +1<CR>")
bind("i", "<A-Up>", "<C-o>:m -2<CR>")
bind("i", "<A-Down>", "<C-o>:m +1<CR>")

-- Open new tab
bind("n", "<C-t>", ":tabnew<CR>")
bind("i", "<C-t>", "<C-o>:tabnew<CR>")

-- Close tab
-- bind("n", "<C-w>", ":bdelete<CR>")
-- bind("i", "<C-w>", "<ESC>:bdelete<CR>")

-- Delete current line (send current line to 'blackhole' register)
bind("n", "<C-S-k>", '"_dd')
bind("i", "<C-S-k>", '<C-o>"_dd')
bind("v", "<C-S-k>", '"_d')

-- Delete current selection/character (send to 'blackhole' register)
bind({ "n", "v" }, "<Del>", '"_x')

-- Toggle comment selection (requires Comment.nvim plugin)
bind("n", "<C-/>", "Vgc")
bind("i", "<C-/>", "<C-o>Vgc")

-- Navigate tabs
bind("n", "<A-n>", ":tabnext<CR>")
bind("i", "<A-n>", "<ESC>:tabnext<CR>")
bind("n", "<A-e>", ":tabprevious<CR>")
bind("i", "<A-e>", "<ESC>:tabprevious<CR>")

-- Search/replace
bind("n", "<C-h>", ":%s/")
bind("i", "<C-h>", "<ESC>:%s/")

-- Save file
bind("n", "<C-s>", ":w<CR>")
bind("i", "<C-s>", "<ESC>:w<CR>")

-- Clear search highlighting
bind("n", "<leader>h", ":noh<CR>")

-- Title-case selection
bind("v", "<leader>t", ":s/\\<./\\u&/g<CR> <bar> :noh<CR>")

-- Navigate backward by one word
bind({ "n", "v" }, "W", "b")

-- Sort lines
bind({ "n", "v" }, "<F9>", "<ESC>ggVG:sort<CR>")

-- Insert timestamp
bind({ "n", "i" }, "<F3>", "<ESC>:InsertSnipDatetime200601311230<CR>")
