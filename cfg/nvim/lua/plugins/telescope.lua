-- Telescope
return {
  "nvim-telescope/telescope.nvim",
  keys = {
    -- Disable leader+Space keybinding (so it won't get stolen from 
    -- `o` FTerm)
    {"<leader><space>", false},
  },
}

