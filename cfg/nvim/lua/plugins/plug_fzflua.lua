-- FZF within Neovim
return {
  "ibhagwan/fzf-lua",
  opts = {
    files = {
      cmd = "fd --type f . ~/working",
    },
  },
}
