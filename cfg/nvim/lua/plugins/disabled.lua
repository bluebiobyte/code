-- disable plugins
return {
  { "folke/noice.nvim", enabled = false },
  -- { "echasnovski/mini.pairs", enabled = false },
  { "ibhagwan/fzf-lua", enabled = false },
  { "jalvesaq/Nvim-R", enabled = false },
}
