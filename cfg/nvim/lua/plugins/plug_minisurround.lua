return {
  -- disable flash.nvim since it hijacks s key
  { "folke/flash.nvim", enabled = false },

  -- remap surround: gz --> s
  {
    "echasnovski/mini.surround",
    event = "BufRead",
    keys = function(_, keys)
      local mappings = {
        { "sa", desc = "Add surrounding", mode = { "n", "v" } },
        { "sd", desc = "Delete surrounding" },
        { "sf", desc = "Find right surrounding" },
        { "sF", desc = "Find left surrounding" },
        { "sh", desc = "Highlight surrounding" },
        { "sr", desc = "Replace surrounding" },
      }
      return vim.list_extend(mappings, keys)
    end,
    config = function(_, _)
      require("mini.surround").setup({})
    end,
  },
}
