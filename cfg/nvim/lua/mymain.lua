-- Dependencies:
--  dtach
--  foot

-- Variables

local opt = vim.opt

-- General functions

function esc_to_normal_mode()
  -- current_mode = "V" means visual line mode, "v" is visual character mode,
  -- and visual block mode is something else. "n" is normal mode.
  local current_mode = vim.api.nvim_get_mode().mode
  if current_mode == "n" then
    return
  end

  local esc_key = vim.api.nvim_replace_termcodes("<ESC>", true, false, true)
  vim.api.nvim_feedkeys(esc_key, current_mode, false)
end

function os.capture(cmd)
  -- Run a given system shell command and return its output
  local f = assert(io.popen(cmd, "r"))
  local out = assert(f:read("*a"))
  f:close()
  -- Strip off leading and trailing spaces and strip trailing newlines
  out = string.gsub(out, "^%s+", "")
  out = string.gsub(out, "%s+$", "")
  out = string.gsub(out, "\n*$", "")
  return out
end

-- Options

-- ...

-- Commands

-- vim.cmd([[command ReloadConfig source $MYVIMRC]]) -- TODO I need to make
-- this work with lua-based config (since I migrated from init.vim)

vim.cmd([[command HardWrap normal! gqap]])

vim.cmd([[command UnwrapPara echom "Use `:join`"]])

vim.cmd([[command -range=% WordCount <line1>,<line2>w !wc -w]])
-- TODO pass character-wise selection, rather than line-based

-- Snips

function insert_snip_at_cursor(opts, snip)
  -- Insert given snippet of text at current cursor position

  -- TODO delete selection (if there is one) first, before inserting snip
  -- see https://www.reddit.com/r/neovim/comments/kkjz7h/get_visual_selection_with_with_lua/
  -- see https://www.reddit.com/r/neovim/comments/oo97pq/how_to_get_the_visual_selection_range/
  -- note that lua string.len() function may be useful

  local ypos, xpos = unpack(vim.api.nvim_win_get_cursor(0))
  local line = vim.api.nvim_get_current_line()
  local new_line = line:sub(0, xpos) .. snip .. line:sub(xpos + 1)
  vim.api.nvim_set_current_line(new_line)
  local new_xpos = xpos + string.len(snip)
  vim.api.nvim_win_set_cursor(0, { ypos, new_xpos })
end

-- function append_snip_at_cursor(snip)
-- Would inject given snippet of text on right side of current cursor
-- position

function insert_datetime_dbY(opts)
  local datetime = os.date("%d %b %Y")
  insert_snip_at_cursor(opts, datetime)
end
vim.api.nvim_create_user_command( -- equvalent to vim's `:command`
  "InsertSnipDatetime02Jan2006",
  insert_datetime_dbY,
  { nargs = "?", range = "%" }
)

function insert_datetime_YmdHM(opts)
  local datetime = os.date("%Y-%m-%d %H:%M")
  insert_snip_at_cursor(opts, datetime)
end
vim.api.nvim_create_user_command( -- equvalent to vim's `:command`
  "InsertSnipDatetime200601311230",
  insert_datetime_YmdHM,
  { nargs = "?", range = "%" }
)

function title_case_line(opts)
  -- TODO once I figure out how to get visual selection and overwrite it, this
  -- function will operate on character-wise selections (instead of just
  -- lines, as it does now)
  vim.cmd([['<,'>s/\v<(.)(\w*)/\u\1\L\2/g]])
  vim.cmd("noh")
end
vim.api.nvim_create_user_command( --
  "TitleCaseLine",
  title_case_line,
  { nargs = "?", range = true }
)

-- Configure plugins

-- FTerm: file opener
local fterm = require("FTerm")
local fterm_o = fterm:new({
  cmd = function()
    local CWD = vim.fn.getcwd()
    local cmd = { "o", "--cache-mode", vim.fn.getcwd() }
    if CWD ~= vim.env.HOME then
      table.insert(cmd, vim.env.HOME)
    end
    return cmd
  end,
  auto_close = true,
  clear_env = false,
})
-- n = normal mode, i = insert mode, t = terminal mode
-- vim.keymap.set({ "n", "t" }, "<A-Space>", function()
vim.keymap.set({ "n", "t" }, "<leader><space>", function()
  fterm_o:toggle()
end)

-- FTerm: file browser
-- NOTE use <leader>E instead
-- local fterm_nnn = fterm:new({
--   cmd = "n", -- My `n` wrapper for `nnn`
--   auto_close = true,
--   clear_env = false,
-- })
-- vim.keymap.set({ "n", "t" }, "<A-b>", function()
--   fterm_nnn:toggle()
-- end)

-- FTerm: compile commands
local runners = {
  -- Use `:set filetype?` to determine current buffer filetype
  lua = { "lua" },
  python = { "python3" },
  r = { "R", "--vanilla", "--quiet", "-f" },
  go = { "go", "run" },
  markdown = { "preview" },
  tex = { "preview" },
  context = { "preview" },
  typst = { "preview" },
}

vim.keymap.set({ "n", "i" }, "<F7>", function()
  esc_to_normal_mode()
  vim.cmd(":w")
  -- TODO pipe buffer contents to compiler commands instead of saving file
  -- and giving them file path
  -- Actually, that might not be good for certain languages that depend on
  -- other files (such as pandoc needing to know where an image is for
  -- markdown) and for compilers that cannot accept content via stdin.

  local file_type = vim.bo.filetype
  local exec = runners[file_type]
  if exec ~= nil then
    local file_path = vim.api.nvim_buf_get_name(0)
    table.insert(exec, file_path)
    require("FTerm").scratch({ cmd = exec })
  end
end)

-- REPL integration

local repl_list = {
  -- Use `:set filetype?` to determine current buffer filetype
  lua = { "lua" },
  python = { "ipython3" },
  r = { "R", "--vanilla", "--quiet", "--interactive" },
  -- r = { "conda", "run", "--name", "base", "R --vanilla --quiet --interactive" },
  sh = { "bash" },
}

vim.keymap.set({ "n", "i", "v" }, "<C-CR>", function()
  -- Get start and end lines of selection (in normal mode, just returns
  -- current line as both start and end)
  local pos_start = vim.fn.getpos("v") -- visual selection start position
  -- In normal mode, 'v' pos = '.' pos
  local pos_end = vim.fn.getpos(".") -- current cursor position
  local pos_start_line = pos_start[2]
  local pos_end_line = pos_end[2]

  -- Determine filetype of current buffer and select appropriate REPL exe
  local buff_file_type = vim.bo.filetype
  local repl = repl_list[buff_file_type]
  if not repl then
    -- If no REPL executable is found for the given buffer's filetype, abort
    return
  end

  -- If REPL session is not already running for this buffer, start one
  if not vim.b.repl_dtach_sock_path then
    local tmp_dir = os.capture("mktemp -d")
    vim.b.repl_dtach_sock_path = tmp_dir .. "/dtach.sock"
    os.execute("dtach -n " .. vim.b.repl_dtach_sock_path .. " " .. table.concat(repl, " "))
    os.execute("foot dtach -a " .. vim.b.repl_dtach_sock_path .. " &")
  end

  -- Send current line/selection to REPL
  local line_range = pos_start_line .. "," .. pos_end_line
  vim.cmd(":silent " .. line_range .. "write !dtach " .. "-p " .. vim.b.repl_dtach_sock_path)
end)

-- Customize indentation rules for Python and R files
-- To see more about options, see `:help ft-python-indent`
-- and see https://neovim.io/doc/user/indent.html
vim.cmd([[
  let g:python_indent = {}
  let g:python_indent.open_paren = 'shiftwidth() * 2'
  let r_indent_align_args = 0
]])
