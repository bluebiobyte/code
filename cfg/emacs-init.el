
; Disable splash screen
(setq inhibit-splash-screen t)


; Keybindings
(global-set-key (kbd "C-z") 'undo)
; (global-set-key (kbd "<f3>") 'isearch-forward)
; (global-set-key (kbd "S-<f3>") 'isearch-backward)
(global-set-key (kbd "<f10>") 'visual-line-mode)
