# Nushell Config File
#
# version = "0.98.0"
#
# Dependencies:
#   zoxide
#   eza


# Compare this config with the default Nushell config
# config nu --default


# PROFILE

$env.VISUAL = "hx"
$env.EDITOR = $env.VISUAL
$env.PAGER = "less"
$env.LESS = "--RAW-CONTROL-CHARS --ignore-case --jump-target=.3 --mouse -XF"
$env.LESSEDIT = "%E < %f"
$env.DEFAULT_TERM = "foot"
$env.STARSHIP_LOG = "error"  # Disable Starship warning messages
if ":ow=01;33" not-in $env.LS_COLORS {
  $env.LS_COLORS = $env.LS_COLORS + ":ow=01;33"  # Color 777 orange
}
if "HOSTNAME" not-in $env {
	$env.HOSTNAME = (hostname)
}

# Daniel's Constant Variables
$env.DCV_CODE_PATH = ([$env.HOME code] | path join)
$env.DCV_HOME_OPT  = ([$env.HOME .local/opt] | path join)

# XDG
$env.XDG_CONFIG_HOME     = ([$env.HOME .config] | path join)
$env.XDG_CACHE_HOME      = ([$env.HOME .cache] | path join)
$env.XDG_DATA_HOME       = ([$env.HOME .local/share] | path join)
$env.XDG_DESKTOP_DIR     = ([$env.HOME working] | path join)
$env.XDG_DOCUMENTS_DIR   = ([$env.HOME progeny] | path join)
$env.XDG_DOWNLOAD_DIR    = ([$env.HOME downloads] | path join)
$env.XDG_MUSIC_DIR       = ([$env.HOME store/music] | path join)
$env.XDG_PICTURES_DIR    = ([$env.HOME record] | path join)
$env.XDG_VIDEOS_DIR      = ([$env.HOME record] | path join)
$env.XDG_TEMPLATES_DIR   = ([$env.HOME code/tplt] | path join)
$env.XDG_PUBLICSHARE_DIR = ([$env.HOME union/public] | path join)
$env.XDG_SCREENSHOTS_DIR = (
    [$env.XDG_PICTURES_DIR screencaptures/current] | path join
)

# Program Config
$env.BAT_PAGER   = $env.PAGER
$env.GOPATH      = ([$env.HOME progeny/go] | path join)
$env.R_LIBS_USER = ([$env.HOME .local/lib/R] | path join)
$env.ZDOTDIR     = ([$env.XDG_CONFIG_HOME zsh] | path join)

# Path
$env.PATH = ($env.PATH | split row (char esep) | append [
    /opt/bin
    ([$env.HOME .local/bin] | path join)
    ([$env.DCV_CODE_PATH bin] | path join)
    ([$env.DCV_CODE_PATH rbin] | path join)
    ([$env.DCV_HOME_OPT bin] | path join)
    ([$env.HOME .cargo/bin] | path join)
    ([$env.GOPATH bin] | path join)
    ([$env.HOME .juliaup/bin] | path join)
    ([$env.HOME union/gis-utils] | path join)
] | uniq)


# CONFIG & OTHER ENV

# Setup prompt
def create_left_prompt [] {
    let dir = match (do --ignore-errors { $env.PWD | path relative-to $nu.home-path }) {
        null => $env.PWD
        '' => '~'
        $relative_pwd => ([~ $relative_pwd] | path join)
    }

    let path_color = (if (is-admin) { ansi red_bold } else { ansi green_bold })
    let separator_color = (if (is-admin) { ansi light_red_bold } else { ansi light_green_bold })
    let path_segment = $"($path_color)($dir)"

    $path_segment | str replace --all (char path_sep) $"($separator_color)(char path_sep)($path_color)"
}

def create_right_prompt [] {
    let time_segment = ([
        (ansi reset)
        (ansi magenta)
        (date now | format date '%Y/%m/%d %R:%S')
        ] |
            str join |
            str replace --regex --all "([/:])" $"(ansi green)${1}(ansi magenta)" |
            str replace --regex --all "([AP]M)" $"(ansi magenta_underline)${1}"
    )

    let last_exit_code = if ($env.LAST_EXIT_CODE != 0) {([
        (ansi rb)
        ($env.LAST_EXIT_CODE)
    ] | str join)
    } else { "" }

    ([$last_exit_code, (char space), $time_segment] | str join)
}

$env.PROMPT_COMMAND = {|| create_left_prompt }
$env.PROMPT_COMMAND_RIGHT = {|| create_right_prompt }


# Create classic var name aliases for PATH and HOME
let HOME = $env.HOME
let PATH = $env.PATH
let USER = $env.USER
let TERM = $env.TERM


# Theme
let dark_theme = {
    # color for nushell primitives
    separator: white
    leading_trailing_space_bg: { attr: n } # no fg, no bg, attr none effectively turns this off
    header: green_bold
    empty: blue
    # Closures can be used to choose colors for specific values.
    # The value (in this case, a bool) is piped into the closure.
    # eg) {|| if $in { 'light_cyan' } else { 'light_gray' } }
    bool: light_cyan
    int: white
    filesize: cyan
    duration: white
    date: purple
    range: white
    float: white
    string: white
    nothing: white
    binary: white
    cell-path: white
    row_index: green_bold
    record: white
    list: white
    block: white
    hints: dark_gray
    search_result: { bg: red fg: white }
    shape_and: purple_bold
    shape_binary: purple_bold
    shape_block: blue_bold
    shape_bool: light_cyan
    shape_closure: green_bold
    shape_custom: green
    shape_datetime: cyan_bold
    shape_directory: cyan
    shape_external: cyan
    shape_externalarg: green_bold
    shape_external_resolved: light_yellow_bold
    shape_filepath: cyan
    shape_flag: blue_bold
    shape_float: purple_bold
    # shapes are used to change the cli syntax highlighting
    shape_garbage: { fg: white bg: red attr: b }
    shape_glob_interpolation: cyan_bold
    shape_globpattern: cyan_bold
    shape_int: purple_bold
    shape_internalcall: cyan_bold
    shape_keyword: cyan_bold
    shape_list: cyan_bold
    shape_literal: blue
    shape_match_pattern: green
    shape_matching_brackets: { attr: u }
    shape_nothing: light_cyan
    shape_operator: yellow
    shape_or: purple_bold
    shape_pipe: purple_bold
    shape_range: yellow_bold
    shape_record: cyan_bold
    shape_redirection: purple_bold
    shape_signature: green_bold
    shape_string: green
    shape_string_interpolation: cyan_bold
    shape_table: blue_bold
    shape_variable: purple
    shape_vardecl: purple
    shape_raw_string: light_purple
}

# The default config record
$env.config = {
    show_banner: false # false to disable the startup welcome banner

    ls: {
        use_ls_colors: true # use the LS_COLORS environment variable to colorize output
        clickable_links: true # enable or disable clickable links. Your terminal has to support links.
    }

    rm: {
        always_trash: false # always act as if -t was given. Can be overridden with -p
    }

    table: {
        mode: light # basic, compact, compact_double, light, thin, with_love, rounded, reinforced, heavy, none, other
        index_mode: auto # "always" show indexes, "never" show indexes, "auto" = show indexes when a table has "index" column
        show_empty: false # show 'empty list' and 'empty record' placeholders for command output
        padding: { left: 1, right: 1 } # a left right padding of each column in a table
        trim: {
            methodology: wrapping # wrapping or truncating
            wrapping_try_keep_words: true # A strategy used by the 'wrapping' methodology
            truncating_suffix: "..." # A suffix used by the 'truncating' methodology
        }
        header_on_separator: false # show header text on separator/border line
        # abbreviated_row_count: 10 # limit data rows from top and bottom after reaching a set point
    }

    error_style: "fancy" # "fancy" or "plain" for screen reader-friendly error messages

    # Whether an error message should be printed if an error of a certain kind is triggered.
    display_errors: {
        exit_code: false # assume the external command prints an error message
        # Core dump errors are always printed, and SIGPIPE never triggers an error.
        # The setting below controls message printing for termination by all other signals.
        termination_signal: true
    }

    # datetime_format determines what a datetime rendered in the shell would look like.
    # Behavior without this configuration point will be to "humanize" the datetime display,
    # showing something like "a day ago."
    datetime_format: {
        # normal: '%a, %d %b %Y %H:%M:%S %z'    # shows up in displays of variables or other datetime's outside of tables
        # table: '%m/%d/%y %I:%M:%S%p'          # generally shows up in tabular outputs such as ls. commenting this out will change it to the default human readable datetime format
    }

    explore: {
        status_bar_background: { fg: "#1D1F21", bg: "#C4C9C6" },
        command_bar_text: { fg: "#C4C9C6" },
        highlight: { fg: "black", bg: "yellow" },
        status: {
            error: { fg: "white", bg: "red" },
            warn: {}
            info: {}
        },
        selected_cell: { bg: light_blue },
    }

    history: {
        max_size: 100_000 # Session has to be reloaded for this to take effect
        sync_on_enter: true # Enable to share history between multiple sessions, else you have to close the session to write history to file
        file_format: "sqlite" # "sqlite" or "plaintext"
        isolation: true # only available with sqlite file_format. true will allow the history to be isolated to the current session using up/down arrows. false will allow the history to be shared across all sessions.
  }

    completions: {
        case_sensitive: false # set to true to enable case-sensitive completions
        quick: true    # set this to false to prevent auto-selecting completions when only one remains
        partial: true    # set this to false to prevent partial filling of the prompt
        algorithm: "fuzzy"    # prefix or fuzzy
        sort: "smart" # "smart" (alphabetical for prefix matching, fuzzy score for fuzzy matching) or "alphabetical"
        external: {
            enable: true # set to false to prevent nushell looking into $env.PATH to find more suggestions, `false` recommended for WSL users as this look up may be very slow
            max_results: 64 # setting it lower can improve completion performance at the cost of omitting some options
            completer: null # check 'carapace_completer' above as an example
        }
        use_ls_colors: true # set this to true to enable file/path/directory completions using LS_COLORS
    }

    filesize: {
        unit: "metric"
    }

    cursor_shape: {
        emacs: line # block, underscore, line, blink_block, blink_underscore, blink_line, inherit to skip setting cursor shape (line is the default)
        vi_insert: block # block, underscore, line, blink_block, blink_underscore, blink_line, inherit to skip setting cursor shape (block is the default)
        vi_normal: underscore # block, underscore, line, blink_block, blink_underscore, blink_line, inherit to skip setting cursor shape (underscore is the default)
    }

    color_config: $dark_theme # if you want a more interesting theme, you can replace the empty record with `$dark_theme`, `$light_theme` or another custom record
    footer_mode: 25 # always, never, number_of_rows, auto
    float_precision: 4 # the precision for displaying floats in tables
    buffer_editor: null # command that will be used to edit the current line buffer with ctrl+o, if unset fallback to $env.EDITOR and $env.VISUAL
    use_ansi_coloring: true
    bracketed_paste: true # enable bracketed paste, currently useless on windows
    edit_mode: emacs # emacs, vi
    shell_integration: {
        # osc2 abbreviates the path if in the home_dir, sets the tab/window title, shows the running command in the tab/window title
        osc2: true
        # osc7 is a way to communicate the path to the terminal, this is helpful for spawning new tabs in the same directory
        osc7: true
        # osc8 is also implemented as the deprecated setting ls.show_clickable_links, it shows clickable links in ls output if your terminal supports it. show_clickable_links is deprecated in favor of osc8
        osc8: true
        # osc9_9 is from ConEmu and is starting to get wider support. It's similar to osc7 in that it communicates the path to the terminal
        osc9_9: false
        # osc133 is several escapes invented by Final Term which include the supported ones below.
        # 133;A - Mark prompt start
        # 133;B - Mark prompt end
        # 133;C - Mark pre-execution
        # 133;D;exit - Mark execution finished with exit code
        # This is used to enable terminals to know where the prompt is, the command is, where the command finishes, and where the output of the command is
        osc133: true
        # osc633 is closely related to osc133 but only exists in visual studio code (vscode) and supports their shell integration features
        # 633;A - Mark prompt start
        # 633;B - Mark prompt end
        # 633;C - Mark pre-execution
        # 633;D;exit - Mark execution finished with exit code
        # 633;E - Explicitly set the command line with an optional nonce
        # 633;P;Cwd=<path> - Mark the current working directory and communicate it to the terminal
        # and also helps with the run recent menu in vscode
        osc633: true
        # reset_application_mode is escape \x1b[?1l and was added to help ssh work better
        reset_application_mode: true
    }
    render_right_prompt_on_last_line: false # true or false to enable or disable right prompt to be rendered on last line of the prompt.
    use_kitty_protocol: false # enables keyboard enhancement protocol implemented by kitty console, only if your terminal support this.
    highlight_resolved_externals: false # true enables highlighting of external commands in the repl resolved by which.
    recursion_limit: 50 # the maximum number of times nushell allows recursion before stopping it

    plugins: {} # Per-plugin configuration. See https://www.nushell.sh/contributor-book/plugins.html#configuration.

    plugin_gc: {
        # Configuration for plugin garbage collection
        default: {
            enabled: true # true to enable stopping of inactive plugins
            stop_after: 10sec # how long to wait after a plugin is inactive to stop it
        }
        plugins: {
            # alternate configuration for specific plugins, by name, for example:
            #
            # gstat: {
            #     enabled: false
            # }
        }
    }

    hooks: {
        pre_prompt: [{ null }] # run before the prompt is shown
        pre_execution: [{ null }] # run before the repl input is run
        env_change: {
            PWD: [{|before, after| null }] # run if the PWD environment is different since the last repl input
        }
        display_output: "if (term size).columns >= 100 { table -e } else { table }" # run to display the output of a pipeline
        command_not_found: { null } # return an error message when a command is not found
    }

    menus: [
        # Configuration for default nushell menus
        # Note the lack of source parameter
        {
            name: completion_menu
            only_buffer_difference: false
            marker: "| "
            type: {
                layout: columnar
                columns: 1
                col_width: 20     # Optional value. If missing all the screen width is used to calculate column width
                col_padding: 2
            }
            style: {
                text: green
                selected_text: { attr: r }
                description_text: yellow
                match_text: { attr: u }
                selected_match_text: { attr: ur }
            }
        }
        {
            name: ide_completion_menu
            only_buffer_difference: false
            marker: "| "
            type: {
                layout: ide
                min_completion_width: 0,
                max_completion_width: 50,
                max_completion_height: 10, # will be limited by the available lines in the terminal
                padding: 0,
                border: true,
                cursor_offset: 0,
                description_mode: "prefer_right"
                min_description_width: 0
                max_description_width: 50
                max_description_height: 10
                description_offset: 1
                # If true, the cursor pos will be corrected, so the suggestions match up with the typed text
                #
                # C:\> str
                #      str join
                #      str trim
                #      str split
                correct_cursor_pos: false
            }
            style: {
                text: green
                selected_text: { attr: r }
                description_text: yellow
                match_text: { attr: u }
                selected_match_text: { attr: ur }
            }
        }
        {
            name: history_menu
            only_buffer_difference: true
            marker: "? "
            type: {
                layout: list
                page_size: 10
            }
            style: {
                text: green
                selected_text: green_reverse
                description_text: yellow
            }
        }
        {
            name: help_menu
            only_buffer_difference: true
            marker: "? "
            type: {
                layout: description
                columns: 4
                col_width: 20     # Optional value. If missing all the screen width is used to calculate column width
                col_padding: 2
                selection_rows: 4
                description_rows: 10
            }
            style: {
                text: green
                selected_text: green_reverse
                description_text: yellow
            }
        }
        # Non-default menus
        {
            name: abbr_menu
            only_buffer_difference: false
            marker: "👀 "
            type: {
                layout: columnar
                columns: 1
                col_width: 20
                col_padding: 2
            }
            style: {
                text: green
                selected_text: green_reverse
                description_text: yellow
            }
            source: {|buffer, position|
                scope aliases
                | where name == $buffer
                | each { |it| {value: $it.expansion }}
            }
        }
    ]

    keybindings: [
        {
            name: abbr
            modifier: control
            keycode: space
            mode: [emacs, vi_normal, vi_insert]
            event: [
                { send: menu name: abbr_menu }
                { edit: insertchar, value: ' '}
            ]
        }
        {
            name: completion_menu
            modifier: none
            keycode: tab
            mode: [emacs vi_normal vi_insert]
            event: {
                until: [
                    { send: menu name: completion_menu }
                    { send: menunext }
                    { edit: complete }
                ]
            }
        }
        {
            name: ide_completion_menu
            modifier: control
            keycode: char_n
            mode: [emacs vi_normal vi_insert]
            event: {
                until: [
                    { send: menu name: ide_completion_menu }
                    { send: menunext }
                    { edit: complete }
                ]
            }
        }
        {
            name: history_menu
            modifier: control
            keycode: char_r
            mode: [emacs, vi_insert, vi_normal]
            event: { send: menu name: history_menu }
        }
        {
            name: help_menu
            modifier: none
            keycode: f1
            mode: [emacs, vi_insert, vi_normal]
            event: { send: menu name: help_menu }
        }
        {
            name: completion_previous_menu
            modifier: shift
            keycode: backtab
            mode: [emacs, vi_normal, vi_insert]
            event: { send: menuprevious }
        }
        {
            name: next_page_menu
            modifier: control
            keycode: char_x
            mode: emacs
            event: { send: menupagenext }
        }
        {
            name: undo_or_previous_page_menu
            modifier: control
            keycode: char_z
            mode: emacs
            event: {
                until: [
                    { send: menupageprevious }
                    { edit: undo }
                ]
            }
        }
        {
            name: escape
            modifier: none
            keycode: escape
            mode: [emacs, vi_normal, vi_insert]
            event: { send: esc }    # NOTE: does not appear to work
        }
        {
            name: cancel_command
            modifier: control
            keycode: char_c
            mode: [emacs, vi_normal, vi_insert]
            event: { send: ctrlc }
        }
        {
            name: quit_shell
            modifier: control
            keycode: char_d
            mode: [emacs, vi_normal, vi_insert]
            event: { send: ctrld }
        }
        {
            name: clear_screen
            modifier: control
            keycode: char_l
            mode: [emacs, vi_normal, vi_insert]
            event: { send: clearscreen }
        }
        {
            name: search_history
            modifier: control
            keycode: char_q
            mode: [emacs, vi_normal, vi_insert]
            event: { send: searchhistory }
        }
        {
            name: open_command_editor
            modifier: control
            keycode: char_o
            mode: [emacs, vi_normal, vi_insert]
            event: { send: openeditor }
        }
        {
            name: move_up
            modifier: none
            keycode: up
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: menuup }
                    { send: up }
                ]
            }
        }
        {
            name: move_down
            modifier: none
            keycode: down
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: menudown }
                    { send: down }
                ]
            }
        }
        {
            name: move_left
            modifier: none
            keycode: left
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: menuleft }
                    { send: left }
                ]
            }
        }
        {
            name: move_right_or_take_history_hint
            modifier: none
            keycode: right
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: historyhintcomplete }
                    { send: menuright }
                    { send: right }
                ]
            }
        }
        {
            name: move_one_word_left
            modifier: control
            keycode: left
            mode: [emacs, vi_normal, vi_insert]
            event: { edit: movewordleft }
        }
        {
            name: move_one_word_right_or_take_history_hint
            modifier: control
            keycode: right
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: historyhintwordcomplete }
                    { edit: movewordright }
                ]
            }
        }
        {
            name: move_to_line_start
            modifier: none
            keycode: home
            mode: [emacs, vi_normal, vi_insert]
            event: { edit: movetolinestart }
        }
        {
            name: move_to_line_start
            modifier: control
            keycode: char_a
            mode: [emacs, vi_normal, vi_insert]
            event: { edit: movetolinestart }
        }
        {
            name: move_to_line_end_or_take_history_hint
            modifier: none
            keycode: end
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: historyhintcomplete }
                    { edit: movetolineend }
                ]
            }
        }
        {
            name: move_to_line_end_or_take_history_hint
            modifier: control
            keycode: char_e
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: historyhintcomplete }
                    { edit: movetolineend }
                ]
            }
        }
        {
            name: move_to_line_start
            modifier: control
            keycode: home
            mode: [emacs, vi_normal, vi_insert]
            event: { edit: movetolinestart }
        }
        {
            name: move_to_line_end
            modifier: control
            keycode: end
            mode: [emacs, vi_normal, vi_insert]
            event: { edit: movetolineend }
        }
        {
            name: move_up
            modifier: control
            keycode: char_p
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: menuup }
                    { send: up }
                ]
            }
        }
        {
            name: move_down
            modifier: control
            keycode: char_t
            mode: [emacs, vi_normal, vi_insert]
            event: {
                until: [
                    { send: menudown }
                    { send: down }
                ]
            }
        }
        {
            name: delete_one_character_backward
            modifier: none
            keycode: backspace
            mode: [emacs, vi_insert]
            event: { edit: backspace }
        }
        {
            name: delete_one_word_backward
            modifier: control
            keycode: backspace
            mode: [emacs, vi_insert]
            event: { edit: backspaceword }
        }
        {
            name: delete_one_character_forward
            modifier: none
            keycode: delete
            mode: [emacs, vi_insert]
            event: { edit: delete }
        }
        {
            name: delete_one_character_forward
            modifier: control
            keycode: delete
            mode: [emacs, vi_insert]
            event: { edit: delete }
        }
        {
            name: delete_one_character_backward
            modifier: control
            keycode: char_h
            mode: [emacs, vi_insert]
            event: { edit: backspace }
        }
        {
            name: delete_one_word_backward
            modifier: control
            keycode: char_w
            mode: [emacs, vi_insert]
            event: { edit: backspaceword }
        }
        {
            name: move_left
            modifier: none
            keycode: backspace
            mode: vi_normal
            event: { edit: moveleft }
        }
        {
            name: newline_or_run_command
            modifier: none
            keycode: enter
            mode: emacs
            event: { send: enter }
        }
        {
            name: move_left
            modifier: control
            keycode: char_b
            mode: emacs
            event: {
                until: [
                    { send: menuleft }
                    { send: left }
                ]
            }
        }
        {
            name: move_right_or_take_history_hint
            modifier: control
            keycode: char_f
            mode: emacs
            event: {
                until: [
                    { send: historyhintcomplete }
                    { send: menuright }
                    { send: right }
                ]
            }
        }
        {
            name: redo_change
            modifier: control
            keycode: char_g
            mode: emacs
            event: { edit: redo }
        }
        {
            name: undo_change
            modifier: control
            keycode: char_z
            mode: emacs
            event: { edit: undo }
        }
        {
            name: paste_before
            modifier: control
            keycode: char_y
            mode: emacs
            event: { edit: pastecutbufferbefore }
        }
        {
            name: cut_word_left
            modifier: control
            keycode: char_w
            mode: emacs
            event: { edit: cutwordleft }
        }
        {
            name: cut_line_to_end
            modifier: control
            keycode: char_k
            mode: emacs
            event: { edit: cuttolineend }
        }
        {
            name: cut_line_from_start
            modifier: control
            keycode: char_u
            mode: emacs
            event: { edit: cutfromstart }
        }
        {
            name: swap_graphemes
            modifier: control
            keycode: char_t
            mode: emacs
            event: { edit: swapgraphemes }
        }
        {
            name: move_one_word_left
            modifier: alt
            keycode: left
            mode: emacs
            event: { edit: movewordleft }
        }
        {
            name: move_one_word_right_or_take_history_hint
            modifier: alt
            keycode: right
            mode: emacs
            event: {
                until: [
                    { send: historyhintwordcomplete }
                    { edit: movewordright }
                ]
            }
        }
        {
            name: move_one_word_left
            modifier: alt
            keycode: char_b
            mode: emacs
            event: { edit: movewordleft }
        }
        {
            name: move_one_word_right_or_take_history_hint
            modifier: alt
            keycode: char_f
            mode: emacs
            event: {
                until: [
                    { send: historyhintwordcomplete }
                    { edit: movewordright }
                ]
            }
        }
        {
            name: delete_one_word_forward
            modifier: alt
            keycode: delete
            mode: emacs
            event: { edit: deleteword }
        }
        {
            name: delete_one_word_backward
            modifier: alt
            keycode: backspace
            mode: emacs
            event: { edit: backspaceword }
        }
        {
            name: delete_one_word_backward
            modifier: alt
            keycode: char_m
            mode: emacs
            event: { edit: backspaceword }
        }
        {
            name: cut_word_to_right
            modifier: alt
            keycode: char_d
            mode: emacs
            event: { edit: cutwordright }
        }
        {
            name: upper_case_word
            modifier: alt
            keycode: char_u
            mode: emacs
            event: { edit: uppercaseword }
        }
        {
            name: lower_case_word
            modifier: alt
            keycode: char_l
            mode: emacs
            event: { edit: lowercaseword }
        }
        {
            name: capitalize_char
            modifier: alt
            keycode: char_c
            mode: emacs
            event: { edit: capitalizechar }
        }
        # The following bindings with `*system` events require that Nushell has
        # been compiled with the `system-clipboard` feature.
        # If you want to use the system clipboard for visual selection or to
        # paste directly, uncomment the respective lines and replace the version
        # using the internal clipboard.
        {
            name: copy_selection
            modifier: control_shift
            keycode: char_c
            mode: emacs
            event: { edit: copyselection }
            # event: { edit: copyselectionsystem }
        }
        {
            name: cut_selection
            modifier: control_shift
            keycode: char_x
            mode: emacs
            event: { edit: cutselection }
            # event: { edit: cutselectionsystem }
        }
        # {
        #     name: paste_system
        #     modifier: control_shift
        #     keycode: char_v
        #     mode: emacs
        #     event: { edit: pastesystem }
        # }
        {
            name: select_all
            modifier: control_shift
            keycode: char_a
            mode: emacs
            event: { edit: selectall }
        }
    ]
}


# Aliases
alias py = python3
alias ipy = ipython --no-confirm-exit -i
alias p = wl-paste
alias lo2pdf = libreoffice --headless --invisible --convert-to pdf
alias lo2txt = libreoffice --headless --invisible --convert-to txt
alias cl = ^cal -3
alias myip = echo "Use `lsip`"
alias noise = play -r48000 -c2 -n synth -1 pinknoise .1 60
alias r = R --no-save
alias log = svlogtail
alias lswifi = nmcli device wifi list
alias lsfonts = fc-list ":" family
alias progress = progress ...[
    --monitor
    --additional-command -lf
    --additional-command nu
    --additional-command pdal
    --additional-command R
]
alias prog = progress
alias diff = diff -u
alias df = df -h
alias lo = libreoffice
alias wpwsh = `/mnt/c/Program Files/PowerShell/7/pwsh.exe`
alias time = time -p
alias b = bash -c
alias lsblk = lsblk -o (
    'NAME,SIZE,FSSIZE,FSAVAIL,FSUSE%,MOUNTPOINTS,FSTYPE,LABEL,PARTLABEL,'
    + 'MODEL,ROTA,TRAN'
)
alias pg = pgrep -fa
alias zyp = sudo zypper
alias ihead = sed 1d
alias inv-head = ihead
alias inverse-head = ihead
alias rm-csv-header = ihead
alias drop-first-line = ihead
alias lsdbus = dbus-send ...[
    --print-reply
    --dest=org.freedesktop.DBus
    /org/freedesktop/DBus
    org.freedesktop.DBus.ListNames
]

alias sav = save
alias text = to text
alias txt = to text
alias cols = columns
alias len = length
alias sel = select
alias hist = histogram
alias stdv = math stddev
alias mean = math avg
alias max = math max
alias min = math min
alias "math mean" = math avg
alias "math var" = math variance
alias l = lines
alias lns = lines
alias sortby = sort-by
alias srtby = sort-by
alias strcap = str capitalize
alias strcat = str join
alias strin = str contains
alias strip = str trim
alias strlen = str length
alias strlow = str downcase
alias strrev = str reverse
alias strsub = str replace --all --regex --multiline
alias sub = strsub
alias re.sub = strsub
alias strup = str upcase
alias "split col" = split column

alias cp = cp --progress
alias mv = mv --progress
alias rm = rm -v

alias nu-ls = ls
alias ls = eza --icons=auto
alias ll = eza --icons=auto --long
alias lst = eza --icons=auto --long --sort=modified
alias lss = eza --icons=auto --long --sort=size
alias lsp = eza --classify=never --color=never --icons=never  # Plain format

alias nu-echo = echo
alias echo = ^echo

# alias nu-sort = sort
# alias nu-uniq = uniq
# alias srt = sort
# alias unq = uniq
# alias sort = ^sort
# alias uniq = ^uniq

alias nu-watch = watch
def watch [cmd: closure, wait = 1sec] {
    # For list of tput codes, see `man 5 terminfo`
    clear
    print --no-newline $"(tput civis)"  # Set cursor invisible
    do --ignore-errors { loop {
        let text_update = (
            $"(tput home)(date now)(char newline)(do $cmd)(tput ed)"
        )
        print --no-newline $text_update
        # let msg = $"(tput home)(date now)\n(do $cmd)"
        # $msg | lines | each {|line|
        #   # Print message line, clear to end of line, then print \n
        #   print --no-newline (tput cud1) (tput cr) $line (tput el)
        # }
        # print --no-newline (tput ed)  # Clear from cursor to end of screen
        sleep $wait
    }}
}
def fwatch [cmd: closure, ...pths: string] {
    $pths | par-each {|p|
        nu-watch $p {echo (date now); do $cmd}
    }
}

alias nu-mkdir = mkdir
def mkdir [--parents (-p), ...args] {
    nu-mkdir ...$args
}

def lsip [] {
    ip -json -4 addr | from json | select ifname addr_info.local | flatten
}
def c [] {
    # Shortcut to pipe content to the clipboard
    to text | wl-copy
}
def ls_pth_exes [] {
    bash -c "compgen -c" | lines
}
def whichedit [pth_exe: string@ls_pth_exes] {
    ^$env.VISUAL (which $pth_exe | get path | first)
}
alias we = whichedit
def ppid [pid: string] {
    cat $"/proc/($pid)/stat" | cut -d ' ' -f 4
}
def timediff [time1: string, time2: string] {
    # Print the time delta between two datetimes
    ($time2 | into datetime) - ($time1 | into datetime)
}
def page [pid: string] {
    # Get process age
    # Output is [[dd-]hh:]mm:ss, where 'dd' is day(s)
    ^ps -p $pid -o etime
}
def s [host_alias: string] {
    match $host_alias {
        "m" => { exec ssh-host mesa },
        "z" => { exec ssh-host zion },
        "r" => { exec ssh-host rocky },
        "h" => { exec ssh-host hpc },
        _   => { echo "SSH remote host alias not defined" },
    }
}
def gcat [commit: string, path: string] {
        # Cat file from specific git commit
        mut path = $path
        if not ($path | str starts-with "/") {
            $path = "./" + $path
        }
        git show $"($commit):($path)"
}
def xargs [cmd: closure, --chunk-size (-c) = 10000]: list -> list {
    # This function expects list as input stream and outputs a list
    #
    # Example usage:
    # - fd . /usr | xargs {len}
    # - fd . /usr | xargs {echo ...$in | ^head -c 100}

    let content = ($in)
    let content_type = ($in | describe | split row '<' | get 0)
    if ($content | describe --no-collect) in ["byte stream" string] {
        ($content 
        | lines
        | window $chunk_size --stride $chunk_size --remainder
        | each $cmd
        | flatten
        )
    } else {
        ($content 
        | window $chunk_size --stride $chunk_size --remainder
        | each $cmd
        | flatten
        )
    }
}
def protectf [file_list: list<path>] {
    $file_list | xargs {|chunk| sudo chattr +i ...$chunk}
}
def tl [dir_pth: path] {
    # Tail latest (most recently modified) log file under given directory
    let log_pth = (eza --absolute --sort=time $dir_pth | lines | last)
    print $"Tailing log: ($log_pth)"
    ^tail --follow $log_pth
}
def basename [paths, suff: string] {
    let $suff = ($suff | str replace --all '.' '\.')
    $paths | path basename | str replace --all --regex $"($suff)$" '' 
}
alias nu-du = du
def du [first_path = `.`: path, ...more_paths: path] {
    let sizes = (nu-ls
        --du 
        $first_path ...$more_paths
        | select size name
    )
    let sum = $sizes | get size | math sum
    nu-echo ($sizes | append {name: TOTAL, size: $sum})
}
# def fd [...args] {
#   ^fd ...$args | lines
# }
def tailf [path: path, ...args] {
    ^tail --follow ...$args $path
}
def lsc [] {
    ^ls | wc --lines 
}
def wcl [] {
    text | wc -l
}
def chunk [chunk_size = 10000] {
    window $chunk_size --stride $chunk_size --remainder
}
def fd-sm [query, search_path = ['.']] {
    # Find and sort by last modified time

    # fd -I gpkg | l|nu-ls ...$in|sortby modified | get modified | format date "%Y-%m-%d %H:%M:%S"

    let paths = (
        fd --no-ignore --follow $query ...$search_path | lines
    )
    let mod_times = (stat -c %y ...$paths | lines | wrap modified)

    $mod_times | merge ($paths | wrap path) | sort-by modified
}
def rp [...path: path] {
    realpath ...$path
}
def run-cmd-array [...cmd_arr: list] {
    run-external ($cmd_arr | first) ...($cmd_arr | skip 1)
}
def psub [cmd: list, ...sub_cmds: closure] {
    let tmpdir = (mktemp -d)
    let index = ($sub_cmds | enumerate | select index)
    let out_paths = (
        $index.index | each {|i| [$tmpdir $i] | path join | wrap out_path }
    )
    let sub_cmds_table = (
        $sub_cmds | wrap cmd | merge $index | merge $out_paths
    )
    for it in $sub_cmds_table {
        do $it.cmd | save $it.out_path
    }
    run-cmd-array ...$cmd ...$sub_cmds_table.out_path
    ^rm -fr $tmpdir | ignore
}
def --wrapped head [...args] {
    # The --wrapped flag interprets all arguments as strings, I believe (so
    # Nushell will not throw a flag error when an argument is passed that starts
    # with '-')
    text | ^head ...$args
}
def --wrapped tail [...args] {
    text | ^tail ...$args
}
def --wrapped fork [...args] {
    bash -c '"$@" &> /dev/null &' bash ...$args
}
alias "&" = fork

extern can [
    ...paths: glob
]
extern o [
    ...paths: glob
]

def qgis [...paths: glob] {
    # If you want to pass flags like '--help' to qgis, use ^qgis (since this
    # alias function is meant to easily open files with an app in the
    # background)
    fork $"($HOME)/code/bin/qgis" ...$paths
}
def gimp [...paths: glob] {
    fork gimp ...$paths
}
def imv [...paths: glob] {
    fork imv ...$paths
}
def zathura [...paths: glob] {
    fork zathura ...$paths
}

def printer-ls [] {
    lpstat -p
}
def printer-cancel [printer_name: string] {
    lprm -P $printer_name
}
def printer-print [printer_name: string, page_range: string, pdf_path: path] {
    # lp -d Zebra-Technologies-ZTC-ZD421-203dpi-ZPL -o 4x3 -P "$pages" "$1"
    lp -d $printer_name -P $page_range $pdf_path
}
def rsynca [...paths: glob] {
    (rsync 
        --archive
        --human-readable
        --compress
        --progress
        --backup
        ...$paths
    )
}


# Zoxide
if not ("~/.cache/zoxide.nu" | path exists) {
    zoxide init nushell | save -f ~/.cache/zoxide.nu
}
source ~/.cache/zoxide.nu
alias d = z
alias di = zi


# Starship (prompt)
starship init nu | save -f ~/.cache/starship-init.nu
use ~/.cache/starship-init.nu
