# Daniel's Fish Config

# Dependencies: moreutils (for `ifdata`), gio, dict, progress, pstree,
#               starship, NerdFonts (for starship, at least), zoxide


# USER TIPS:
# You can navigate forward and back throgh 'cd' history via the use of the
# ALT+<LEFT_ARROW> and ALT+<RIGHT_ARROW> keys combinations.


# Load zoxide (autojump utility)
zoxide init --cmd d fish | source


# Load starship (prompt setter)
starship init fish | source


# Disable fish greeting
set fish_greeting ""


# Aliases
# Once abbreviations are removed from here, they may also need to be purged
# from ~/.config/fish/fish_variables (sometimes they stick around in there).
# Lines starting with "SETUVAR _fish_abbr_*" would be the relevant ones. All
# such lines can be removed, as fish will just add abbreviations from this
# file back to that one once the shell is reloaded.

# Aliases: Universal (aliases I use on all my systems)
abbr --add py 'python3'
abbr --add ipy 'ipython'
abbr --add c 'wl-copy'  # Shortcut to pipe content to the clipboard
abbr --add p 'wl-paste'
abbr --add m 'micro'
abbr --add s 'subl'
abbr --add lo2pdf "libreoffice --headless --invisible --convert-to pdf"
abbr --add lo2txt "libreoffice --headless --invisible --convert-to txt"
abbr --add cl "cal -3"
abbr --add yd 'youtube-dl'
abbr --add myip "ifdata -pa $DFLT_NET_IFACE"
abbr --add noise "play -r48000 -c2 -n synth -1 pinknoise .1 60"
abbr --add bs 'python "$DCV_CODE_PATH/bin/backup-$HOSTNAME.py"'
abbr --add 20s 'sleep 1;' \
    'mpv --volume=50 --video=no ~/downloads/20\ SECOND\ TIMER.mkv'
abbr --add pdfcat 'pdfunite'
abbr --add pg 'progress --monitor'
abbr --add lscpt 'pstree -s -p %self'  # List current process tree
# Launch new session with history
abbr --add hoff \
    'DCV_SHELL_PROMPT_MSG="$DCV_SHELL_PROMPT_MSG PRIVATE" fish --private'
# Connect to my phone through via mosh over SSH over tailscale (VPN)
abbr --add ssh-oneplus 'mosh --ssh="ssh -p 8022" 100.106.145.69'
abbr --add mntusb 'echo "Use '"'"'mntm'"'"'"'
abbr --add ls 'exa'
abbr --add r 'R --no-save'
abbr --add lswifi 'nmcli device wifi list'
abbr --add lsfonts 'fc-list : family'

# Aliases: Local (aliases that are system-specific)
if test "$HOSTNAME" = "daspirecu"
    abbr --add btlvl 'echo (cat /sys/class/power_supply/BAT1/capacity)%'
    abbr --add reboot "sudo runit-init 6"
    abbr --add hs "cat ~/.local/share/fish/fish_history" \
        "~/.local/share/fish/fish_history.archive | rg -i"

else if test "$HOSTNAME" = "dtideapad"
    abbr --add btlvl 'upower -i' \
        '/org/freedesktop/UPower/devices/battery_BAT1 |' \
        'rg -e percentage -e state'

else if test "$HOSTNAME" = "andtermuxd"
    abbr --add o 'termux-open'
    abbr --add c 'termux-clipboard-set'
    abbr --add p 'termux-clipboard-get'

end
