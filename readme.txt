
# Hierarchy

	bin:
		Executable scripts.

	cfg:
		Configuration files. Normally, such files would go under ~/.config but sometimes a configuration file is critical to my normal work-flow (I modify it often) and I want it in an easy-to-access location. Other reasons I may choose to put configuration files here include the contents being very important and me not wanting to risk them getting lost with other dot files when I migrate to a new system.

		Especially if a configuration/settings file does not belong to a specific program (it is used by several) or if it is not automatically read by a program and needs to be passed as a flag (such as a white-list or black-list), then such a file belongs in this directory.

	etc:
		Custom system-wide config files that go under /etc.

	rbin:
		"Real" bin (for executable binary files). "Bin" on Linux has come to mean simply "the place for executable files." This directory is for true compiled binary executables (not executable text files, such as scripts). These files are stored in this directory so that it can be excluded from the git repo (to reduce repo size) but still included in PATH. The source files for these binaries are under `src`. Thus, anyone who clones this repository can recreate the binaries themselves.

	sbin:
		Executables that I do not want in my PATH. Executables that belong here are ones that I would not normally call directly and that I do not want executed by accident. For instance, the script that starts my window manager is run automatically once I login, and I would rarely call this script via an interactive shell.

	src:
		Source files for binaries under `rbin`.

	tplt:
		Templates.


# Git: Procuring Changes From Remote

	cd ~/code/
	git checkout BRANCH  # optional
	git status  # make sure local changes are comitted locally
	git fetch REMOTE
	git merge REMOTE/BRANCH -m MESSAGE


# Git: Cleaning up all the temporary cursory commits

	git reset --soft COMMIT_ID
	git add .
	git commit -m 'tmp'

	This will uncommit all of the changes commited since the commit made before COMMIT_ID and then commit the current state of your working directory (which is the state it was in since the last cursory commit) as one singular commit.

	COMMIT_ID is the commit *just before* the oldest one you want to uncommit. Commit IDs can be found in the git log.
