#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Daniel Rode
# Name:
# Tags:
# Dependencies:
#   Python 3.10+
#   dep2
#   dep3
#   ...
# Version: 1
# Init:
# Updated: -


# Description:
# ...
# ...


import os
import sys
from pathlib import Path
from sys import exit


# Constants
PROGRAM_NAME = "[Unique Name of Program]"
EXE_NAME = sys.argv[0].split('/')[-1]  # This script's filename
HELP_TEXT = f"Usage: {EXE_NAME} [OPTION]... ARG"

# Variables: XDG Base Directory (user data paths)
if 'XDG_CONFIG_HOME' in os.environ:
    XDG_CONFIG_HOME = Path(os.environ['XDG_CONFIG_HOME'])
else:
    XDG_CONFIG_HOME = Path.home() / '.config'
CONFIG_HOME = XDG_CONFIG_HOME / 'daniel_rode_code' / PROGRAM_NAME
CONFIG_HOME.mkdir(parents=True, exist_ok=True)

if 'XDG_CACHE_HOME' in os.environ:
    XDG_CACHE_HOME = Path(os.environ['XDG_CACHE_HOME'])
else:
    XDG_CACHE_HOME = Path.home() / '.cache'
CACHE_HOME = XDG_CACHE_HOME / 'daniel_rode_code' / PROGRAM_NAME
CACHE_HOME.mkdir(parents=True, exist_ok=True)

if 'XDG_DATA_HOME' in os.environ:
    XDG_DATA_HOME = Path(os.environ['XDG_DATA_HOME'])
else:
    XDG_DATA_HOME = Path.home() / '.local/share'
DATA_HOME = XDG_DATA_HOME / 'daniel_rode_code' / PROGRAM_NAME
DATA_HOME.mkdir(parents=True, exist_ok=True)


# Functions
def print2(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)


# Main
def main():
    # Parse command line arguments
    args = iter(sys.argv[1:])
    pos_args = []
    for a in args:
        if not a.startswith('-'):
            pos_args += [a]
            continue
        match i:
            case '-p' | '--path':
                path = next(args)
            case '-d' | '--do':
                do_the_thing = True
            case _:
                print("error: Invalid flag", a)
                exit(1)

    try:
        main_arg1, main_arg2 = pos_args
    except ValueError:
        print(HELP_TEXT)
        exit(1)

    # Read content from file if path is provided, otherwise, read from stdin
    try:
        path = Path(sys.argv[1])
    except IndexError:
        file_content = sys.stdin.read()
    else:
        with open(path, 'r') as f:
            file_content = f.read()

    # Use concurrency via parallelism to run a task in the background
    from concurrent.futures import ProcessPoolExecutor

    def go(func, value):
        executor = ProcessPoolExecutor(max_workers=1)
        worker = executor.submit(func, value)
        executor.shutdown(wait=False)

        return worker

    worker = go(sum, [1,2,3,4])  # non-blocking
    result = worker.result()  # blocking

    # Use parallel concurrency to run several tasks in the background
    from concurrent.futures import ProcessPoolExecutor, as_completed

    def dispatch(job_list, worker):
        with ProcessPoolExecutor() as executor:
            worker_list = { executor.submit(worker, j): j for j in job_list }
            for future in as_completed(worker_list):
                job = worker_list[future]
                result = future.result()
                yield job, result

    def worker(job):
        return job**3

    jobs = [10,20,30,40]
    for job, result in dispatch(jobs, worker):
        # Results are ordered by which finish first
        print(job, result)

    # Style: Chain method calls
    result = (
        function
        .method(something)
        .method2(another_thing)
        .last_method()
    )

    # Style: Long with blocks (requires Python 3.10+)
    with (
        mock.patch('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa') as a,
        mock.patch('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb') as b,
        mock.patch('cccccccccccccccccccccccccccccccccccccccccc') as c,
    ):
        # do stuff


if __name__ == '__main__':
    main()



"""
TODO
- task 1
- task 2
- ...
"""
