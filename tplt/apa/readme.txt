
TODO
`essay-apa.md` is my old APA template. It should be converted to use the
approach the following writing project...
/home/daniel/union/edu/ESS 440-001 (Practicing Sustainability)/assignments/0223 background research paper

Consider including pantable as dependency and perhaps any other dependencies, methods used by my `preview` script.

For in-text citation format (in markdown), see:
- https://frederikaust.com/papaja_man/writing.html#citations
- https://pandoc.org/MANUAL.html#citations-in-note-styles

Regarding citing figures, see:
- https://github.com/lierdakil/pandoc-crossref  # dependency pandoc-crossref
