
# This template is more of a meta template for me to use as I learn to navigate the Python science libraries. It will contain snippets of code I find useful. As I become more familiar, I will start to actually structure the code in this file into separate scripts designed to fulfill a specific purpose (such as boxplot of excel data, etc...).

import pandas as pd
import matplotlib.pyplot as plt


# see '/home/daniel/edu/STAT 302A-001 (Statistics Supplement)/assignments/module 03 assignment/plot.py'

# see '/home/daniel/edu/ESS 129-001 (Information Management for Sustainability)/assignments/05 Time Series Analysis of Public Domain Datasets/plot.py'

# see plots for ESS 330 class


# for csv, use `pd.read_csv` instead of `pd.read_excel`
# pd.read_excel
    # parameter sheet_name=
# for both read_csv and read_excel, the skiprows parameter can be used to ignore the first N rows of the spreadsheet when importing the data


# import excel column as set of years
# pd.read_excel('./Bipolar.ods', sheet_name='positive')
data = pd.read_csv('./path.csv')
x = pd.to_datetime(data['Year'], format="%Y")


# plot types
fig, ax = plt.subplots()
ax.boxplot([column1, column2, ...], labels=x_ticks_labels)  # boxplot
ax.plot(x,y)  # standard plot


# show plot
plt.show()


# BASIC PLOT EXAMPLE

EXPORT_FILE_FORMAT = 'svg'
EXPORT_FILE_NAME = f"plot.{EXPORT_FILE_FORMAT}"
DATA_PATH = "./downloads/ESS129_Module04_Discussion02_Stevens.csv"
SHEET_NAME = "Sheet 1"
COLUMNS = [
    'Year',
    'Incidence (per 100,000 people) ',
]
PLOT_TITLE = None
X_LABEL = "Year"
Y_LABEL = "Incidence (per 100,000 people)"
X_TICK_RANGE = [0, 1400]
Y_TICK_RANGE = [210, 280]


# Import data
data = pd.read_csv(DATA_PATH, skiprows=6, sheet_name=SHEET_NAME)
data = pd.DataFrame(data[COLUMNS])


# Boxplot: Select data
x = pd.to_datetime(data['Year'], format="%Y")
y = data['Incidence (per 100,000 people) ']


# Boxplot: Plot
fig, ax = plt.subplots()
ax.plot(x,y)  # Create plot
ax.set(
    title=PLOT_TITLE,  # Label plot
    xlabel=X_LABEL,  # Label x-axis
    ylabel=Y_LABEL,  # Label y-axis
    xlim=X_TICK_RANGE, # Set max and min x-ticks
    ylim=Y_TICK_RANGE, # Set max and min y-ticks
)


# Boxplot: Save plot
# fig.savefig(EXPORT_FILE_NAME)
plt.show()
