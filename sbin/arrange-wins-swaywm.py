#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Daniel Rode
# Dependencies:
#   Python3.7+
#   i3ipc
#   hx
#   foot
#   firefox
#   htop
# Created: 26 Dec 2022
# Updated: 28 Jan 2025


# Description: Start list of defined programs under Sway session and then
# arrange their windows. Then arrange subsequent windows in spiral pattern.


import i3ipc


# Functions
def arrange_spiral(i3, e):
    # New window hook for arranging all windows opened after initial set

    # TODO
    # As of 20 Mar 2023, this funciton is splitting windows weird and I do not
    # know why, so for now, I will simply disable it.
    exit()

    # con = i3.get_tree().find_by_id(e.container.id)  # con = container
    con = i3.get_tree().find_focused()  # con = container
    if con.type == 'floating_con':
        return
    if con.fullscreen_mode == 1:
        return
    if con.layout in ["stacked", "tabbed"]:
        return

    if con.rect.height > con.rect.width:
        i3.command("splitv")
    else:
        i3.command("splith")


def arrange_initial_windows():
    # Arrange initial windows openend when Sway first starts

    i3.command("layout tabbed")

    hx_con_id = hx_containers[0].id
    i3.command(f"[con_id={hx_con_id}] move right")
    i3.command(f"[con_id={hx_con_id}] move right")
    i3.command(f"[con_id={hx_con_id}] move right")

    term_con_id = term_containers[0].id
    i3.command(f"[con_id={term_con_id}] layout splitv")


def on_new_win(i3, e):
    # New window hook for arranging windows opened on Sway init

    if e.container.app_id == "foot":
        term_containers.append(e.container)

    elif e.container.app_id == "foot_helix":
        hx_containers.append(e.container)

    if (len(term_containers) == 2) and (len(hx_containers) == 1):
        i3.off(on_new_win)
        arrange_initial_windows()

        i3.on(i3ipc.Event.WINDOW_NEW, arrange_spiral)


# Main
term_containers = []
hx_containers = []

i3 = i3ipc.Connection()
i3.on(i3ipc.Event.WINDOW_NEW, on_new_win)

i3.command("workspace 2")
i3.command("exec firefox")
i3.command("workspace 1")
i3.command("exec foot-helix ~/union/notes/wiki/program/helix.txt")
i3.command("exec foot -e nu")
i3.command("exec foot -e htop")

i3.main()
