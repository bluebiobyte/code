#!/usr/bin/env dash
# Dependencies:
#   brightnessctl
#   dropbox
#   syncthing
#   gocryptfs
#   dbus
# Updated: 20 Jun 2024


# Startup commands

# Startup: Aspire One
if [ "$HOSTNAME" = "daspirecu" ]
then
	sudo brightnessctl --quiet set 100

# Startup: IdeaPad
elif [ "$HOSTNAME" = "dtideapad" ]
then
	# If Dropbox is not running, start it
	if ! pidof dropbox > /dev/null 2>&1
	then
		dropbox start &
	fi
fi

# Startup: All

# If Syncthing is not running, start it
if ! pidof syncthing > /dev/null 2>&1
then
	syncthing -no-browser -audit \
		-auditfile="$HOME/.local/var/log/syncthing-audit.log" \
		2>&1 >> "$HOME/.local/var/log/syncthing-main.log" &
fi

# Set variables for Sway
export _JAVA_AWT_WM_NONREPARENTING=1
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export MOZ_ENABLE_WAYLAND=1
export XDG_SESSION_TYPE=wayland
export XDG_CURRENT_DESKTOP=sway


# Launch Sway
echo "Initializing Sway..."
exec dbus-run-session sway
